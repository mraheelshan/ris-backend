<?php

namespace App\Listeners\User;

use App\Events\User\UserCreated;

class UserCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        //


        //return false; // stop propogation to other events
    }
}
