<?php

namespace App\Helpers;

use GuzzleHttp\Client;


class MessageHelper {

	public static function send($to,$message)
	{
 		$client = new Client();

        $res = $client->request('POST', 'https://secure.h3techs.com/sms/api/send', [
            'form_params' => [
                'email' => 'pearlfaisal@yahoo.com',
                'key' => '120b96fb610b4cf852e4562c87355e98b8',
                'mask' => 'NY Store',
                'to' => $to,
                'message' => $message
            ]
        ]);

        $data = [
        	'status' => $res->getStatusCode(),
        	'headers' => $res->getHeader('content-type'),
        	'body' => $res->getBody()
        ];
    
    	return $data;
	}
}