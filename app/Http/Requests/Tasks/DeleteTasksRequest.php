<?php

namespace App\Http\Requests\Tasks;

use App\Models\Tasks;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteTasksRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Tasks');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Tasks::find($this->id);

        $item->delete();

        return true;
    }    
}
