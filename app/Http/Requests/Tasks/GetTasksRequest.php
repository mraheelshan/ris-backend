<?php

namespace App\Http\Requests\Tasks;

use App\Models\Tasks;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetTasksRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Tasks');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return Tasks::findOrNew($this->id);
        
    }
}
