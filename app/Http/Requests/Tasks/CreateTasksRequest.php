<?php

namespace App\Http\Requests\Tasks;

use App\Models\Tasks;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateTasksRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Tasks');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        
        $params['user_id'] = auth()->user()->id;
        
        return Tasks::create($params);
    }    
}
