<?php

namespace App\Http\Requests\AdmissionRequest;

use App\Models\AdmissionRequest;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllAdmissionRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-AdmissionRequests');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return AdmissionRequest::all();

    }
}
