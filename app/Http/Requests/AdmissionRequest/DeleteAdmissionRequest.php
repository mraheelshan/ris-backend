<?php

namespace App\Http\Requests\AdmissionRequest;

use App\Models\AdmissionRequest;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteAdmissionRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-AdmissionRequest');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = AdmissionRequest::find($this->id);

        $item->delete();

        return true;
    }    
}
