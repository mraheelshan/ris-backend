<?php

namespace App\Http\Requests\AdmissionRequest;

use App\Models\AdmissionRequest;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateAdmissionRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-AdmissionRequest');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){
        $this->validated();

        $params = $this->all();
        $params['dob'] = date('Y-m-d',strtotime($params['dob']));
        $params['submission_date'] = date('Y-m-d',strtotime($params['submission_date']));
        return AdmissionRequest::where('id', $this->id)->update($params);
    }    
}
