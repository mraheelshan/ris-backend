<?php

namespace App\Http\Requests\Academic;

use App\Models\Academic;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllAcademicRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-Academics');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return Academic::all();

    }
}
