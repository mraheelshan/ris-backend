<?php

namespace App\Http\Requests\Academic;

use App\Models\Academic;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateAcademicRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Academic');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        $params['start_date'] = date('Y-m-d',strtotime($params['start_date']));
        $params['end_date'] = date('Y-m-d',strtotime($params['end_date']));
        
        return Academic::where('id', $this->id)->update($params);
    }    
}
