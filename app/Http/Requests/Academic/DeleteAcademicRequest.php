<?php

namespace App\Http\Requests\Academic;

use App\Models\Academic;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteAcademicRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Academic');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Academic::find($this->id);

        $item->delete();

        return true;
    }    
}
