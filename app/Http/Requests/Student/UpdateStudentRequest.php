<?php

namespace App\Http\Requests\Student;

use App\Models\Student;
use App\Models\StudentParents;
use App\Models\Family;
use App\Http\Requests\BaseRequest;
use Bouncer;
use PHPUnit\Framework\MockObject\Builder\Stub;

class UpdateStudentRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Student');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
    // $offer =  Offer::where('id', $this->id)->update($params);



    // OfferItem::where('offer_id', $this->id)->delete();
    public function handle()
    {

        $this->validated();

        $params = $this->all();

        $student = $params['student'];
        $familyNumber =  Family::where('family_number', $student['family_number'])->first();
        $student['family_id'] =  $familyNumber['id'];
        $student['dob'] = date('Y-m-d', strtotime($student['dob']));
        $student['receipt_date'] = date('Y-m-d', strtotime($student['receipt_date']));
        $student['admission_date'] = date('Y-m-d', strtotime($student['admission_date']));

        $student =  Student::where('id', $this->id)->update($student);


        $parents = $params['parents'];


        unset($params['parents']);
        StudentParents::where('student_id', $this->id)->delete();

        if (sizeof($parents) > 0) {
            foreach ($parents as $item) {
                $item['family_id'] =  $familyNumber['id'];
                $item['dob'] = date('Y-m-d', strtotime($item['dob']));
                $item['student_id'] = $this->id;
                StudentParents::create($item);
            }
        }
        return true;
    }
}
