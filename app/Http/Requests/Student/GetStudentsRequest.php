<?php

namespace App\Http\Requests\Student;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetStudentsRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-School');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {
        $studentResponse = [];
        $student = Student::where('class_id', $this->id)->get();

        if (sizeof($student) > 0) {
            foreach ($student as $value) {

                $row['student_id'] = $value['id'];
                $row['name'] = $value['first_name'];
                $row['class_id'] = $value['classes']->id;
                $row['school_id'] = $value['school']->id;
                array_push($studentResponse, $row);
            }
        }
      
        return $studentResponse;
    }
}
