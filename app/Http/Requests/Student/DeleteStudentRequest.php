<?php

namespace App\Http\Requests\Student;

use App\Models\Student;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteStudentRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Student');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Student::find($this->id);

        $item->delete();

        return true;
    }    
}
