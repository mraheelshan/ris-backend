<?php

namespace App\Http\Requests\Student;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use App\Models\StudentParents;
use Bouncer;

class GetStudentRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Student');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $student =  Student::findOrNew($this->id);
        $parents = StudentParents::where('student_id', $student->id)->get();

        return ['student' => $student , 'parents'=> $parents];
    }
}
