<?php

namespace App\Http\Requests\Attendance;

// use App\Models\Attendance;
use App\Models\Student;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetReportRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Attendance');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $this->validated();
        $params = $this->all();
        $month = date('m', strtotime($params['month']));
        $year =  date('y', strtotime($params['year']));
        $school_id = $params['school_id'];
        $class_id = $params['class_id'];
        $student_id = $params['student_id'];

        $students = Student::where('class_id', $class_id)->where('school_id', $school_id);
        
        if ($student_id != 0) {
            $students->where('id', $student_id);
        }

        $allStudents = $students->get();
        
        $allStudents->map(function ($student) use ($month, $year) {

            $student->month_attendence = $student->getAttendence($month, $year);

            return $student;
        });

        return $allStudents;
    }
}
