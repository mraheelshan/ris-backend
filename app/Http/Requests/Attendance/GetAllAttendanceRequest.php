<?php

namespace App\Http\Requests\Attendance;

use App\Models\TeacherCourses;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllAttendanceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-Attendances');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){
       
        // $id =  auth()->user()->id;
        // return TeacherCourses::where('profile_id',$id)->get();
        

    }
}
