<?php

namespace App\Http\Requests\Attendance;

// use App\Models\Attendance;
use App\Models\Student;
use App\Models\TeacherCourses;
use App\Http\Requests\BaseRequest;
use App\Models\Profile;
use App\Models\School;
use Bouncer;

class GetAcademicYearReportRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Attendance');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $this->validated();
        $params = $this->all();
        $month = date('m', strtotime($params['month']));
        $year =  date('y', strtotime($params['year']));

        $schoolObj = School::where('status' , $params['status']);
        if ($params['donor_id'] != 0) {
            $schoolObj->where('donor_id', $params['donor_id']);
        }

        $schools = $schoolObj->get();

        $schools->map(function ($school) {
            $userArray = TeacherCourses::where('school_id', $school['id'])->get()->pluck('user_id')->toArray();
            $userObj = Profile::whereIn('user_id', $userArray)->get()->pluck('name')->toArray();

            $school->teacher = $userObj;
            $school->classCount = explode(',', $school->classes);

            $male = Student::where('school_id', $school['id'])->where('gender', 'Male')->get();
            $female = Student::where('school_id', $school['id'])->where('gender', 'Female')->get();
            $school->male =  $male->count();
            $school->female = $female->count();
            return $school;
        });

        return $schools;
    }
}
