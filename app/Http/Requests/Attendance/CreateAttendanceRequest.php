<?php

namespace App\Http\Requests\Attendance;

use App\Models\Attendance;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateAttendanceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Attendance');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $this->validated();

        $params = $this->all();
        $date = date('d');
        $month = date('m');
        $classId  = $params[0]['class_id'];
        Attendance::where('day', $date)->where('month', $month)->where('class_id', $classId)->delete();
        if (sizeof($params) > 0) {
            foreach ($params as $value) {
                
                $row['student_id'] = $value['student_id'];
                $row['class_id'] = $value['class_id'];
                $row['school_id'] = $value['school_id'];
                $row['status'] = $value['status'];
                $row['name'] = $value['name'];
                $row['day'] = date('d');
                $row['year'] = date('y');
                $row['month'] = date('m');
                $row['date'] = date('y-m-d');

                if ($value['status'] == 3) {
                    $row['leave_reason'] = $value['leave_reason'];
                }
                Attendance::create($row);
            }
            return true;
        }
    }
}
