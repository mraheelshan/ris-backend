<?php

namespace App\Http\Requests\Attendance;

use App\Models\Attendance;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteAttendanceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Attendance');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        // $item = Attendance::find($this->id);

        // $item->delete();

        // return true;
    }    
}
