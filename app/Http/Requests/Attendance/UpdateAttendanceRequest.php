<?php

namespace App\Http\Requests\Attendance;

use App\Models\Attendance;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateAttendanceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Attendance');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){
//
    }    
}
