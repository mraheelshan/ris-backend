<?php

namespace App\Http\Requests\Attendance;

use App\Models\Student;
use App\Http\Requests\BaseRequest;

use Bouncer;

class GetAcademicMonthReportRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Attendance');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {
        $this->validated();
        $params = $this->all();
        $to = date('m', strtotime($params['to']));
        $from = date('m', strtotime($params['from']));
        $year =  date('y', strtotime($params['year']));
        $school_id = $params['school_id'];
        $class_id = $params['class_id'];

        $students = Student::where('class_id', $class_id)->where('school_id', $school_id)->get();

        $students->map(function ($student) use ($to, $from, $year) {

            $attendance = $student->getMonthAttendence($to, $from, $year);
            $present =  $attendance->where('status', 1);
            $notmention =  $attendance->where('status', 0);
            $student->present  = sizeof($present);
            $student->not_mention  = sizeof($notmention);
        });
        $period['year '] = $params['year'];
        $period['from '] = $from;
        $period['to '] = $to;

        return ['students' => $students, 'period' => $period];
    }
}
