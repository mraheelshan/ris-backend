<?php

namespace App\Http\Requests\Syllabus;

use App\Models\Syllabus;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateSyllabusRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Syllabus');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
      
        return Syllabus::where('id', $this->id)->update($params);
    }    
}
