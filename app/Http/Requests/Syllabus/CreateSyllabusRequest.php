<?php

namespace App\Http\Requests\Syllabus;

use App\Models\Syllabus;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateSyllabusRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Syllabus');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        
        $params['user_id'] = auth()->user()->id;
       
        return Syllabus::create($params);
    }    
}
