<?php

namespace App\Http\Requests\UserType;

use App\Models\UserType;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetUserTypeRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-UserType');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return UserType::findOrNew($this->id);
        
    }
}
