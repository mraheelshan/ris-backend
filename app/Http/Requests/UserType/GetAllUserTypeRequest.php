<?php

namespace App\Http\Requests\UserType;

use App\Models\UserType;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllUserTypeRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-UserTypes');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return UserType::all();

    }
}
