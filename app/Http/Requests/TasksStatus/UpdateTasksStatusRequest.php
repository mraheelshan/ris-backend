<?php

namespace App\Http\Requests\TasksStatus;

use App\Models\Tasks;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateTasksStatusRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true ; // Bouncer::can('update-Offer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        
        return  Tasks::where('id', $this->id)->update(['status' => $params['status']]);
    }    
}
