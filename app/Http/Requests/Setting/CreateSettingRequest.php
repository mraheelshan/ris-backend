<?php

namespace App\Http\Requests\Setting;

use App\Models\Setting;
use App\Models\Banner;
use App\Http\Requests\BaseRequest;
// use Bouncer;

class CreateSettingRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Offer');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {
        Banner::truncate();
        Setting::truncate();

        $this->validated();

        $params = $this->all();

        foreach ($params['url'] as $banner) {

            $row = [
                'url' => $banner,
                'active' => 1,

            ];

            Banner::create($row);
        }

        $row = [
            'type' => 1,
            'title' => $params['title'],
            'products' => implode(',', $params['products']),
            'sort_order' => 1,
        ];

        Setting::create($row);

        $row = [
            'type' => 3,
            'category_id' => $params['category_id'],
            'categories' => implode(',', $params['categories']),
            'sort_order' => 3,

        ];

        Setting::create($row);

        foreach ($params['sections'] as $data) {

            $row = [
                'type' => 2,
                'category_id' => $data['category_id'],
                'products' => implode(',', $data['products']),
                'sort_order' => 2,

            ];

            Setting::create($row);
        }


        return true;
    }
}
