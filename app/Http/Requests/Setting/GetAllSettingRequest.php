<?php

namespace App\Http\Requests\Setting;

use App\Models\Setting;
use App\Http\Requests\BaseRequest;
use App\Models\Banner;
use Bouncer;

class GetAllSettingRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Settings');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
    
    public function handle()
    {

        $banner = Banner::all();
        $param =  Setting::all();
        
        $response = [
            'categories' => [],
            'category_id' => 0,
            'mainCategory' => 0,
            'products' =>  [],
            'sections' => [],
            'title' =>  "",
        ];

        $sections = [];

        foreach ($param as $data) {

            if ($data['type'] === 1) {
                $response['title'] = $data['title'];
                $response['products'] = array_map('intval', explode(',', $data['products']));
            }
            if ($data['type'] == 3) {
                $response['mainCategory'] = $data['category_id'];
                $response['categories'] = array_map('intval', explode(',', $data['categories']));
            }
            if ($data['type'] == 2) {
                $item = [
                    'category_id' => $data['category_id'],
                    'products'  => array_map('intval', explode(',', $data['products']))
                ];
                array_push($sections, $item);
            }
           
        }

        $response['sections'] = $sections;

        return ['data' => $response, 'images' => $banner];
    }
}
