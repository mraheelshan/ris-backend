<?php

namespace App\Http\Requests\Resource;

use App\Http\Requests\BaseRequest;
use App\Core\Response;
use Illuminate\Validation\Rule;

use Storage;

class ImageUploadRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => [
                'required',
                'mimes:jpeg,jpg,png',
                'max:2048',
                Rule::dimensions()->minWidth(100)->minHeight(100)->ratio(1 / 1),
            ],
        ];
    }

    public function handle(){

        $data = $this->validated();

        $path = $this->file('image')->store('temp');

        $path = Storage::url($path);

        $response = new Response();

        $response->payload = url($path);

        return $response;        
    }
}
