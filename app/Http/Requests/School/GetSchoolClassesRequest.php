<?php

namespace App\Http\Requests\School;

use App\Models\School;
use App\Models\Classes;
use App\Models\Course;
use App\Models\TeacherCourses;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetSchoolClassesRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-School');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $response =  School::findOrNew($this->id);
        $classesResponse = [];
        $coursesResponse = [];
        $classes =  explode(',', $response['classes']);

        foreach ($classes as $class) {
            $class = Classes::where('id', $class)->first();
            $courses = TeacherCourses::where('class_id', $class->id)->get();
            if (sizeof($courses) > 0) {
                // $row['class_name'] = $class['name'];
                foreach ($courses as $item) {
                    $course =  Course::findOrNew($item->course_id);
                    $row['course'] = $course['name'];
                    $row['marks'] = $course['marks'];
                    $row['status'] = $course['status'];
                    $row['course_id'] = $course['id'];
                    array_push($coursesResponse, $row);
                }

                $class['courses'] = array_unique($coursesResponse, SORT_REGULAR);
                // $class['courses'] = $coursesResponse;
                $coursesResponse = [];
            } else {
                $class['courses'] = [];
            }
            array_push($classesResponse, $class);
        }
        $response1 = $classesResponse;

        return $response1;
    }
}
