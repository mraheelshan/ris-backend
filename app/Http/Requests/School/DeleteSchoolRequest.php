<?php

namespace App\Http\Requests\School;

use App\Models\School;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteSchoolRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-School');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = School::find($this->id);

        $item->delete();

        return true;
    }    
}
