<?php

namespace App\Http\Requests\School;

use App\Models\School;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetSchoolRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-School');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $response =  School::findOrNew($this->id);
        $response['classes'] = array_map('intval', explode(',', $response['classes']));
        return $response;
    }
}
