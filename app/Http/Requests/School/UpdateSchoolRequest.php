<?php

namespace App\Http\Requests\School;

use App\Models\School;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateSchoolRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-School');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        $params['classes'] = implode(',', $params['classes']);
        return School::where('id', $this->id)->update($params);
    }    
}
