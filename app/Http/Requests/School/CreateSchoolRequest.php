<?php

namespace App\Http\Requests\School;

use App\Models\School;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateSchoolRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-School');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        $params['user_id'] = auth()->user()->id;
        $params['classes'] = implode(',', $params['classes']);
        return School::create($params);
    }    
}
