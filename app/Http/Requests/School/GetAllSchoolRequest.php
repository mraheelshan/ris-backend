<?php

namespace App\Http\Requests\School;

use App\Models\School;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllSchoolRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true; //Bouncer::can('view-Schools');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $params  = School::all();
        foreach ($params as $data) {
            $data['classes'] = array_map('intval', explode(',', $data['classes']));
        }
        return $params;
    }
}
