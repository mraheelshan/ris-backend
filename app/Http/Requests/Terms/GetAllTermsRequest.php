<?php

namespace App\Http\Requests\Terms;

use App\Models\Terms;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllTermsRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-Termss');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return Terms::all();

    }
}
