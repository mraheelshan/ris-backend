<?php

namespace App\Http\Requests\Terms;

use App\Models\Terms;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteTermsRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Terms');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Terms::find($this->id);

        $item->delete();

        return true;
    }    
}
