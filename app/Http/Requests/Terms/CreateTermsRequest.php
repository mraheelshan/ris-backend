<?php

namespace App\Http\Requests\Terms;

use App\Models\Terms;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateTermsRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Terms');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        $params['start_date'] = date('Y-m-d',strtotime($params['start_date']));
        $params['end_date'] = date('Y-m-d',strtotime($params['end_date']));
        return Terms::create($params);
    }    
}
