<?php

namespace App\Http\Requests\Examination;

use App\Models\StudentMarks;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateAttemptExaminationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Examination');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $this->validated();

        $params = $this->all();
        $examId  = $params[0]['exam_id'];
        $classId  = $params[0]['class_id'];
        $courseId  = $params[0]['course_id'];

        StudentMarks::where('exam_id',$examId)->where('class_id', $classId)->where('course_id', $courseId)->delete();

        if (sizeof($params) > 0) {
            foreach ($params as $value) {
                StudentMarks::create($value);
            }
            return true;
        }
    }
}
