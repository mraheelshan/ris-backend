<?php

namespace App\Http\Requests\Examination;

use App\Models\Examination;
use App\Models\CoursesExamination;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateExaminationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Examination');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $this->validated();

        $params = $this->all();
        $classes = $params['classes'];

        $params['date'] = date('Y-m-d', strtotime($params['date']));
        $params['start_time'] = date('H:i:s', strtotime($params['start_time']));
        $params['end_time'] = date('H:i:s', strtotime($params['end_time']));
        $params['class_id'] = implode(',', $params['class_id']);
        
        $exam =  Examination::create($params);
        if (sizeof($classes) > 0) {
            foreach ($classes as $class) {
                if (!empty($class['courses'])) {
                    $courses = $class['courses'];

                    unset($params['courses']);
                    if (sizeof($courses) > 0) {
                        foreach ($courses as $item) {

                            $item['examination_id'] = $exam->id;
                            $item['school_id'] = $exam->school_id;
                            $item['class_id'] = $class['id'];
                            CoursesExamination::create($item);
                        }
                    }
                }
            }
        }

        return true;
    }
}
