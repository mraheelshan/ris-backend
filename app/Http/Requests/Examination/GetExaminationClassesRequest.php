<?php

namespace App\Http\Requests\Examination;

use App\Models\Classes;
use App\Models\Student;
use App\Models\Course;
use App\Models\TeacherCourses;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use App\Models\CoursesExamination;
use App\Models\School;

class GetExaminationClassesRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Profile');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    { 
        // $id =  auth()->user()->id;
        $class = CoursesExamination::where('examination_id', $this->id)->get();
        $classResponse = [];
        
        if (sizeof($class) > 0) {
            foreach ($class as $item) {
                $classdata =  Classes::findOrNew($item->class_id);
                $schooldata =  School::findOrNew($item->school_id);
                $coursedata =  Course::findOrNew($item->course_id);
                $classdata['school-name'] = $schooldata->name;
                $classdata['school_id'] = $schooldata->id;
                $classdata['course'] = $coursedata->name;
                $classdata['course_id'] = $coursedata->id;
                $classdata['exam_id'] = $this->id;
                array_push($classResponse, $classdata);
            }
            
        }
        return $classResponse;
    }
}
