<?php

namespace App\Http\Requests\Examination;

use App\Models\Examination;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Student;
use App\Models\CoursesExamination;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class getExaminationCoursesRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Examination');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {
        /*
        // $response = Examination::findOrNew($this->id);
        
        $classesResponse = [];
        $coursesResponse = [];
        $studentResponse = [];

        // $classes =  explode(',', $response['class_id']);
        // $response['class_id'] = array_map('intval', explode(',', $response['class_id']));

        // foreach ($classes as $class) {

        $student = Student::where('class_id', $this->id)->get();

        if (sizeof($student) > 0) {
            foreach ($student as $value) {

                $row1['student_id'] = $value['id'];
                $row1['name'] = $value['first_name'];
                $row1['class_id'] = $value['classes']->id;
                $row1['school_id'] = $value['school']->id;
                array_push($studentResponse, $row1);
            }
            $class = Classes::where('id', $student[0]->class_id)->first();
        }
        $courses = CoursesExamination::where('class_id', $this->id)->get();
        if (sizeof($courses) > 0) {
            foreach ($courses as $item) {
                $course =  Course::findOrNew($item->course_id);
                $row['course'] = $course['name'];
                $row['marks'] = $item['marks'];
                $row['status'] = $item['status'];
                $row['course_id'] = $course['id'];
               
                array_push($coursesResponse, $row);
            }
            // $class['courses'] = $coursesResponse;
            $class['students'] = $studentResponse;
            $coursesResponse = [];
            $studentResponse = [];
        } else {
            $class['courses'] = [];
        }
        array_push($classesResponse, $class);
        // }
        $response = $classesResponse;
        return $student;
        */
        $this->validated();
        $params = $this->all();

        $students = Student::where('class_id', $params['class_id'])->where('school_id', $params['school_id'])->get();
        $course = Course::where('id', $params['course_id'])->first();
        $courseResponse = [];
        if (sizeof($students) > 0) {
            foreach ($students as $item) {
                $coursedata['school_id'] = $item->school_id;
                $coursedata['student_id'] = $item->id;
                $coursedata['class_id'] = $item->class_id;
                $coursedata['name'] = $item->first_name;
                $coursedata['total_marks'] = $course->marks;
                $coursedata['course_id'] = $course->id;
                $coursedata['course'] = $course->name;
                $coursedata['exam_id'] = $params['exam_id'];
                array_push($courseResponse, $coursedata);
            }
        }
        return $courseResponse;
    }
}
