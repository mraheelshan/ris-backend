<?php

namespace App\Http\Requests\Examination;

use App\Models\Examination;
use App\Models\Classes;
use App\Models\Course;
use App\Models\CoursesExamination;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetExaminationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Examination');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $response = Examination::findOrNew($this->id);
        $classesResponse = [];
        $coursesResponse = [];
        $classes =  explode(',', $response['class_id']);
        $response['class_id'] = array_map('intval', explode(',', $response['class_id']));
        foreach ($classes as $class) {
            $class = Classes::where('id', $class)->first();
            $courses = CoursesExamination::where('class_id', $class->id)->get();
            if (sizeof($courses) > 0) {
                foreach ($courses as $item) {
                    $course =  Course::findOrNew($item->course_id);
                    $row['course'] = $course['name'];
                    $row['marks'] = $item['marks'];
                    $row['status'] = $item['status'];
                    $row['course_id'] = $course['id'];
                    array_push($coursesResponse, $row);
                }
                $class['courses'] = $coursesResponse;
                $coursesResponse = [];
            } else {
                $class['courses'] = [];
            }
            array_push($classesResponse, $class);
        }
        $response['classes'] = $classesResponse;
        return $response;
    }
}
