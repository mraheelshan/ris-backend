<?php

namespace App\Http\Requests\Examination;

use App\Models\Examination;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllExaminationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-Examinations');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return Examination::all();

    }
}
