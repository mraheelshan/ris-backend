<?php

namespace App\Http\Requests\Examination;

use App\Models\Examination;
use App\Models\CoursesExamination;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteExaminationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Examination');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Examination::find($this->id);

        $item->delete();
        
        CoursesExamination::where('examination_id', $this->id)->delete();

        return true;
    }    
}
