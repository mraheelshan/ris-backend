<?php

namespace App\Http\Requests\Chapter;

use App\Models\Chapter;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteChapterRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Chapter');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Chapter::find($this->id);

        $item->delete();

        return true;
    }    
}
