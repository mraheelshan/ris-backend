<?php

namespace App\Http\Requests\Chapter;

use App\Models\Chapter;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateChapterRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Chapter');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
       
        return Chapter::where('id', $this->id)->update($params);
    }    
}
