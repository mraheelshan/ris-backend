<?php

namespace App\Http\Requests\Chapter;

use App\Models\Chapter;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllChapterRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-Chapters');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return Chapter::all();

    }
}
