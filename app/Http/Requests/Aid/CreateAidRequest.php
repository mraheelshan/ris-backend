<?php

namespace App\Http\Requests\Aid;

use App\Models\Aid;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateAidRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Aid');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        
        return Aid::create($params);
    }    
}
