<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;


class UpdateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'billing_address' => ['required', 'string'],
            'shipping_address' => ['required', 'string'],
        ];
    }

    public function handle()
    {
        $user = $this->user();

        $params = $this->all();

        $user->billing_address = $params['billing_address'];
        $user->shipping_address = $params['shipping_address'];

        $user->save();

        return true;
    }
}
