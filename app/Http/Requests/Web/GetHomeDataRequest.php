<?php

namespace App\Http\Requests\Web;

use App\Models\Shippingmethod;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Banner;
use App\Models\Setting;
use App\Models\Product;
use App\Models\Category;


class GetHomeDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle()
    {
        $banners = Banner::where(['active' => 1])->get();

        $homeSettings = Setting::orderBy('sort_order')->get();

        $rows = [];

        foreach ($homeSettings as $key => $value) 
        {
            $row = [];

            $row['type'] = $value->type;

            if($value->type == 1){
                $row['title'] = $value->title;
                $row['products'] = Product::whereIn('id',explode(',',$value->products))->where('is_main',1)->where('active',1)->get();
            }else if($value->type == 2){
                $category = Category::find($value->category_id);
                $row['title'] = $category->name;
                $row['products'] = Product::whereIn('id',explode(',',$value->products))->where('is_main',1)->where('active',1)->get();
            }else if($value->type == 3){
                $row['title'] = '';
                $row['categories'] = Category::whereIn('id',explode(',',$value->categories))->get();
                $row['category'] = Category::where('id',$value->category_id)->first();
            }else{
                $row['title'] = 'All Products';
                $row['products'] = Product::where('is_main',1)->where('active',1)->get();   
            }

            array_push($rows,$row);
        }

        return [ 'banners' => $banners , 'data' => $rows ];
    }
}
