<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;


class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'city' => ['required', 'string'],
        ];
    }

    public function handle()
    {
        $user = $this->user();

        $params = $this->all();

        $user->name = $params['name'];
        $user->phone = $params['phone'];
        $user->city = $params['city'];

        $user->save();

        return true;
    }
}
