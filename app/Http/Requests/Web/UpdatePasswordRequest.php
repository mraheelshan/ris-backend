<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Rules\MatchOldPassword;


class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_password' => ['required', 'string', 'min:8', 'confirmed'],
            'new_password_confirmation'=>['required'],
            'old_password' => ['required', 'string', 'min:8', new MatchOldPassword],
        ];
    }

    public function handle()
    {

        $params = $this->all();

        $user = $this->user();
        $user->password = Hash::make($params['new_password']);
        $user->save();

        return true;
    }
}
