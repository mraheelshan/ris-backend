<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;


class GetProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle()
    {
        $params = $this->all();
                
        $data = $this->getCategorySearch($params);

        if(sizeof($data) > 0){
            return $data;
        }

        $data = [
            'category' => '',
            'subcategories' => [],
            'products' => [],
            'min' => 0,
            'max' => 0,
            'brands' => [],
            'slug' => ''
        ];

        return $data;
    }

    private function getCategorySearch($params)
    {

        $parts = explode('/',$this->slug);

        $slug = null;

        if(sizeof($parts) > 1){
            $slug = end($parts);
        }else{
            $slug = $this->slug;
        }

        $category = Category::where(['slug' => $slug, 'active' => 1 ])->first();

        $data = [];

        if($category !== null)
        {
            $data['slug'] = $this->slug;
            $data['category'] = $category;
            $data['subcategories'] = Category::where(['active' => 1, 'parent_id' => $category->id ])->get();
            $productsObj = Product::with('category','brand')->where(['category_id' => $category->id, 'active' => 1]);

            if(isset($params['brands'])){
                $brands = explode(',',$params['brands']);
                $productsObj->whereIn('brand_id',$brands);
            }

            if(isset($params['min'])){
                $productsObj->where('selling_price' , '<=', $params['min']);
            }

            if(isset($params['max'])){
                $productsObj->where('selling_price' , '>=', $params['max']);
            }

            if(isset($params['sort'])){
                if($params['sort'] == 1){
                    $productsObj->orderBy('selling_price');
                }else if($params['sort'] == 2){
                    $productsObj->orderByDesc('selling_price');
                }else if($params['sort'] == 3){
                    $productsObj->orderBy('name');
                }else if($params['sort'] == 4){
                    $productsObj->orderByDesc('name');
                }else if($params['sort'] == 5){
                    $productsObj->orderBy('discount');
                }else{
                    $productsObj->orderByDesc('discount');
                }
            }

            $products = $productsObj->get();

            $products->map(function($product){

                $product->original_price = $product->selling_price;

                if( (strtotime($product->valid_till) > (strtotime('Y-m-d'))) && $product->offer )  {
                    
                    $product->selling_price = $product->selling_price - ($product->selling_price / 100 * $product->discount);
                }


                return $product;
            });


            $data['products'] = $products;

            $data['min'] = $products->min('selling_price');
            $data['max'] = $products->max('selling_price');

            $brand_ids = $products->pluck('brand_id');

            $data['brands'] = Brand::whereIn('id',$brand_ids)->get();
        }

        return $data;
    }
}
