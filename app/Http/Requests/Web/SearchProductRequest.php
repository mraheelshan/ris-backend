<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;


class SearchProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle()
    {
        $params = $this->all();

        $data = $this->getKeywordSearch($params);

        if(sizeof($data) > 0){
            return $data;
        }  

        $data = [
            'keyword' => '',
            'subcategories' => [],
            'products' => [],
            'min' => 0,
            'max' => 0,
            'brands' => [],
        ];

        return $data;
    }

    private function getKeywordSearch($params)
    {        
        $productsObj = Product::with('category','brand')->where(['active' => 1])->whereRaw('MATCH (name, description) AGAINST (?)' , [$this->slug]);

        if(isset($params['brands'])){
            $brands = explode(',',$params['brands']);
            $productsObj->whereIn('brand_id',$brands);
        }

        if(isset($params['min'])){
            $productsObj->where('selling_price' , '<=', $params['min']);
        }

        if(isset($params['max'])){
            $productsObj->where('selling_price' , '>=', $params['max']);
        }

        if(isset($params['sort'])){
            if($params['sort'] == 1){
                $productsObj->orderBy('selling_price');
            }else if($params['sort'] == 2){
                $productsObj->orderByDesc('selling_price');
            }else if($params['sort'] == 3){
                $productsObj->orderBy('name');
            }else if($params['sort'] == 4){
                $productsObj->orderByDesc('name');
            }else if($params['sort'] == 5){
                $productsObj->orderBy('discount');
            }else{
                $productsObj->orderByDesc('discount');
            }
        }

        $products = $productsObj->get();        

        $data = [];

        if(sizeof($products) > 0)
        {
            $data['keyword'] = $this->slug;

            $category_ids = $products->only(['brand_id']);

            $data['subcategories'] = Category::where(['active' => 1 ])->whereIn('id',$category_ids)->get();

            $data['products'] = $products;

            $data['min'] = $products->min('selling_price');
            $data['max'] = $products->max('selling_price');

            $brand_ids = $products->pluck('brand_id');

            $data['brands'] = Brand::whereIn('id',$brand_ids)->get();
        }

        return $data;
    }
}
