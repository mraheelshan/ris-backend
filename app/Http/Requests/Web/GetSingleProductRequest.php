<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;


class GetSingleProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle()
    {
        $data = $this->getProductDetail();

        if(sizeof($data) > 0){
            return $data;
        }

        return $data;
    }

    private function getProductDetail()
    {
        $product = Product::with('category','brand')->where(['active' => 1])->where('slug',$this->slug)->first();

        $data = [];

        $data['product'] = $product;

        if(!is_null($product))
        {
            $data['variants'] = Product::where('active',1)->where('variant_group',$product->variant_group)->get();
        }

        return $data;
    }
}
