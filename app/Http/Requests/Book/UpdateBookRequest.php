<?php

namespace App\Http\Requests\Book;

use App\Models\Book;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateBookRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Book');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
       
        return Book::where('id', $this->id)->update($params);
    }    
}
