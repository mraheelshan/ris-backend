<?php

namespace App\Http\Requests\Book;

use App\Models\Book;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteBookRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Book');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Book::find($this->id);

        $item->delete();

        return true;
    }    
}
