<?php

namespace App\Http\Requests\Book;

use App\Models\Book;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateBookRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Book');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
       
        return Book::create($params);
    }    
}
