<?php

namespace App\Http\Requests\Profile;

use App\Models\Profile;
use App\Models\User;
use App\Models\TeacherCourses;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateProfileRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-Profile');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {
        $this->validated();
        $params = $this->all();

        $user = new User();
        $user->name = $params['name'];
        $user->password = Hash::make($params['password']);
        $user->email = $params['email'];
        $user->save();

        $params['dob'] = date('Y-m-d', strtotime($params['dob']));
        $params['user_id'] = $user->id;

        $profile = Profile::create($params);
        if (!empty($params['courses'])) {
            $courses = $params['courses'];

            unset($params['courses']);

            if (sizeof($courses) > 0) {
                foreach ($courses as $item) {
                    $coursesArr = $item['course_id'];
                    foreach ($coursesArr as $course) {

                        $row = [
                            'user_id' => $profile->user_id,
                            'school_id' => $item['school_id'],
                            'class_id' => $item['class_id'],
                            'course_id' => $course,
                        ];

                        TeacherCourses::create($row);
                        unset($item['course']);
                    }
                }
            }
        }
        return true;
    }
}
