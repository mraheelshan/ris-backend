<?php

namespace App\Http\Requests\Profile;

use App\Models\Profile;
use App\Models\Classes;
use App\Models\Student;
use App\Models\TeacherCourses;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use App\Models\School;

class GetProfileClassesRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Profile');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {
        $id =  auth()->user()->id;
        // dd($id);
        $class = TeacherCourses::where('user_id', $id)->get();
        $classResponse = [];
        $attendanceResponse = [];
        if (sizeof($class) > 0) {
            foreach ($class as $item) {
                $classdata =  Classes::findOrNew($item->class_id);
                $schooldata =  School::findOrNew($item->school_id);
                $classdata['school-name'] = $schooldata->name;
                $classdata['school-code'] = $schooldata->code;
                array_push($classResponse, $classdata);
            }
            $classResponse = array_unique($classResponse);
            if (sizeof($classResponse) > 0) {
                foreach ($classResponse as $value) {
                    $student = Student::where('class_id', $value->id)->get();

                    if (sizeof($student) > 0) {
                        $row['school'] = $value['school-name'];
                        $row['school-code'] = $value['school-code'];
                        $row['class'] = $value['name'];
                        $row['class_id'] = $value['id'];
                        $row['students'] = sizeof($student);

                        array_push($attendanceResponse, $row);
                    }
                }
            }
        }

        // $response['students'] = $studentResponse;
        $response['classes'] = $attendanceResponse;
        return $response;
    }
}
// foreach ($class as $item) {
//     $classdata =  Classes::findOrNew($item->class_id);
//     $schooldata =  School::findOrNew($item->school_id);

//     $row['school'] = $schooldata['name'];
//     $row['school-code'] = $schooldata['code'];
//     $row['class'] = $classdata['name'];
    
//     array_push($classResponse, $row);

// }
// $classResponse = array_unique($classResponse);
// $classResponse = $classResponse->unique();
// if (sizeof($classResponse) > 0) {
//     foreach ($classResponse as $value) {
//         $student = Student::where('class_id', $value->id)->get();

//         if (sizeof($student) > 0) {
//             array_push($studentResponse, $student);
//         }
//     }
// }

