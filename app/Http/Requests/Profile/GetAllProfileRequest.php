<?php

namespace App\Http\Requests\Profile;

use App\Models\Profile;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetAllProfileRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  true;//Bouncer::can('view-Profiles');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return Profile::all();

    }
}
