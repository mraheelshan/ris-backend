<?php

namespace App\Http\Requests\Profile;

use App\Models\Profile;
use App\Models\TeacherCourses;
use App\Models\User;
use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Hash;

use Bouncer;

class UpdateProfileRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Profile');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {

        $this->validated();
        $id = auth()->user()->id;
        $params = $this->all();
        $courses = $params['courses'];
        unset($params['courses']);

        $params['dob'] = date('Y-m-d', strtotime($params['dob']));
        $user = Profile::where('id', $this->id)->first();
        $user->update($params);
        $row['name'] = $params['name'];
        $row['email'] = $params['email'];
        $row['password'] = Hash::make($params['password']);
        User::where('id', $user->user_id)->update($row);

        if (sizeof($courses) >  0) {
            TeacherCourses::where('user_id', $user->user_id)->delete();
            if (sizeof($courses) > 0) {
                foreach ($courses as $item) {
                    $coursesArr = $item['course_id'];
                    foreach ($coursesArr as $course) {

                        $row = [
                            'user_id' => $user->user_id,
                            'school_id' => $item['school_id'],
                            'class_id' => $item['class_id'],
                            'course_id' => $course,
                        ];

                        TeacherCourses::create($row);
                        unset($item['course']);
                    }
                }
            }
        }
        return true;
    }
}
