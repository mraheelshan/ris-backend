<?php

namespace App\Http\Requests\Profile;

use App\Models\Profile;
use App\Models\TeacherCourses;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;
use GuzzleHttp\Handler\Proxy;

class GetProfileRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Profile');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function handle()
    {
        $courseResponse = [];
        $courseArr = [];
        $response =  Profile::findOrNew($this->id);

        $courses =  TeacherCourses::where('user_id', $response['user_id'])->get();

        if (sizeof($courses) > 0) {
            foreach ($courses as $item) {

                $Obj = TeacherCourses::where('school_id', $item['school_id'])->where('class_id', $item['class_id'])->get()->pluck('course_id')->toArray();
                $row['school_id'] = $item['school_id'];
                $row['class_id'] = $item['class_id'];
                $row['course_id'] = $Obj;

                array_push($courseResponse, $row);
            }
        }

        $profilecourses = array_unique($courseResponse, SORT_REGULAR);

        $response['classes'] = $profilecourses;
        return $response;
    }
}
