<?php

namespace App\Http\Requests\SchoolTypes;

use App\Models\SchoolTypes;
use App\Http\Requests\BaseRequest;
use Bouncer;

class CreateSchoolTypeRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('add-SchoolType');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        $params['created_at'] = date('Y-m-d',strtotime($params['created_at']));
        $params['updated_at'] = date('Y-m-d',strtotime($params['updated_at']));
        return SchoolTypes::create($params);
    }    
}
