<?php

namespace App\Http\Requests\SchoolClasses;

use App\Models\Classes;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteClassesRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Classes');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Classes::find($this->id);

        $item->delete();

        return true;
    }    
}
