<?php

namespace App\Http\Requests\SchoolClasses;

use App\Models\Classes;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateClassesRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Classes');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){
        $this->validated();

        $params = $this->all();
       
        return Classes::where('id', $this->id)->update($params);
    }    
}
