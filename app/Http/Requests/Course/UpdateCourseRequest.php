<?php

namespace App\Http\Requests\Course;

use App\Models\Course;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateCourseRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Course');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
       
        return Course::where('id', $this->id)->update($params);
    }    
}
