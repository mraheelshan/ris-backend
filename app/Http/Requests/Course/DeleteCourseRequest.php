<?php

namespace App\Http\Requests\Course;

use App\Models\Course;
use App\Http\Requests\BaseRequest;
use Bouncer;

class DeleteCourseRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('delete-Course');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }

    public function handle(){

        $item = Course::find($this->id);

        $item->delete();

        return true;
    }    
}
