<?php

namespace App\Http\Requests\Family;

use App\Models\Family;
use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Bouncer;

class GetFamilyRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('view-Family');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        return Family::findOrNew($this->id);
        
    }
}
