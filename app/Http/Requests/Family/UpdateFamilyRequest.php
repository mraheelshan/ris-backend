<?php

namespace App\Http\Requests\Family;

use App\Models\Family;
use App\Http\Requests\BaseRequest;
use Bouncer;

class UpdateFamilyRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // Bouncer::can('update-Family');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function handle(){

        $this->validated();

        $params = $this->all();
        
        return Family::where('id', $this->id)->update($params);
    }    
}
