<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Tasks\GetAllTasksRequest;
use App\Http\Requests\Tasks\GetTasksRequest;
use App\Http\Requests\Tasks\CreateTasksRequest;
use App\Http\Requests\Tasks\UpdateTasksRequest;
use App\Http\Requests\Tasks\DeleteTasksRequest;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetAllTasksRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id,GetTasksRequest $request)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTasksRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTasksRequest $request, $id)
    {
        $request->id = $id;
        
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteTasksRequest $request,$id)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }  
}