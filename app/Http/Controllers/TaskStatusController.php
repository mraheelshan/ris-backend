<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\TasksStatus\UpdateTasksStatusRequest;


class TaskStatusController extends Controller
{
    
    public function update(UpdateTasksStatusRequest $request, $id)
    {
        $request->id = $id;
        
        $response = $request->handle();

        return response()->json($response);
    }
   
}