<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Examination\GetAllExaminationRequest;
use App\Http\Requests\Examination\GetExaminationRequest;
use App\Http\Requests\Examination\CreateExaminationRequest;
use App\Http\Requests\Examination\UpdateExaminationRequest;
use App\Http\Requests\Examination\DeleteExaminationRequest;
use App\Http\Requests\Examination\getExaminationCoursesRequest;
use App\Http\Requests\Examination\GetExaminationClassesRequest;
use App\Http\Requests\Examination\CreateAttemptExaminationRequest;

class ExaminationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetAllExaminationRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id,GetExaminationRequest $request)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateExaminationRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExaminationRequest $request, $id)
    {
        $request->id = $id;
        
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteExaminationRequest $request,$id)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }
   
    public function getClasses(GetExaminationClassesRequest $request,$id)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }  
    public function getCourses(getExaminationCoursesRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }  
    public function examStore(CreateAttemptExaminationRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }  
   
}