<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Permission\GetAllPermissionsRequest;
use App\Http\Requests\Permission\GetPermissionRequest;
use App\Http\Requests\Permission\CreatePermissionRequest;
use App\Http\Requests\Permission\UpdatePermissionRequest;
use App\Http\Requests\Permission\DeletePermissionRequest;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetAllPermissionsRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
        // if(\Request::wantsJson()) { 
        // } else {
        //     return view('permission.index',[ 'permissions' => $response]);
        // }  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(GetPermissionRequest $request,$id)
    {
        $request->id = $id;

        $response = $request->handle();
        return response()->json($response);

        // if(\Request::wantsJson()) { 
        // } else {
        //     return view('permission.form', ['permission' => $response, 'route' => route('permissions.store'), 'edit' => false ] );
        // }        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePermissionRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
        // if(\Request::wantsJson()) { 
        // } else {
        //     return redirect()->route('permissions.index');
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,GetPermissionRequest $request)
    {
        $request->id = $id;

        $response = $request->handle();

        return response()->json($response);
        // if(\Request::wantsJson()) { 
        // } else {
        //     return view('permission.form', ['permission' => $response, 'route' => route('permissions.update',['id' => $request->id ]), 'edit' => true ] );
        // }        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePermissionRequest $request, $id)
    {
        $request->id = $id;
        
        $response = $request->handle();

        return response()->json($response);
        // if(\Request::wantsJson()) { 
        // } else {
        //     return redirect()->route('permissions.index');
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,DeletePermissionRequest $request)
    {
        $request->id = $id;

        $response = $request->handle();
        return response()->json($response);

        // if(\Request::wantsJson()) { 
        // } else {
        //     return redirect()->route('permissions.index');
        // }
    }  
}