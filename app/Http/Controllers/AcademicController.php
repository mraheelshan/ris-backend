<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Academic\GetAllAcademicRequest;
use App\Http\Requests\Academic\GetAcademicRequest;
use App\Http\Requests\Academic\CreateAcademicRequest;
use App\Http\Requests\Academic\UpdateAcademicRequest;
use App\Http\Requests\Academic\DeleteAcademicRequest;

class AcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetAllAcademicRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id,GetAcademicRequest $request)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAcademicRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAcademicRequest $request, $id)
    {
        $request->id = $id;
        
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteAcademicRequest $request,$id)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }  
}