<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Resource\ImageUploadRequest;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageUploadRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);        
    }

    public function imageUpload(\Illuminate\Http\Request $request)
    {
        $rules = ['image' => 'required|max:40000|dimensions:min_width=150,min_height=150'];
        $messages = [
            'max' => 'The :attribute size can not exceed 4MB',
            'dimensions' => 'The :attribute size must be at least 200 * 200'
        ];

        $validator = Validator::make($request->all(),$rules,$messages);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $file = $request->file('image');

        $path = $request->image->store('images');

        $resposne = [
            'name'  => $file->getClientOriginalName(),
            'path' => url(sprintf('public/storage'.'/'.$path)),
            'size' => $file->getClientSize(),
        ];

        return response()->json($resposne);
    }    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
