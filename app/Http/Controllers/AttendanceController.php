<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\Attendance\GetAllAttendanceRequest;
use App\Http\Requests\Attendance\GetAttendanceRequest;
use App\Http\Requests\Attendance\CreateAttendanceRequest;
use App\Http\Requests\Attendance\UpdateAttendanceRequest;
use App\Http\Requests\Attendance\DeleteAttendanceRequest;
use App\Http\Requests\Attendance\GetReportRequest;
use App\Http\Requests\Attendance\GetMonthWiseReportRequest;
use App\Http\Requests\Attendance\GetAcademicYearReportRequest;
use App\Http\Requests\Attendance\GetAcademicMonthReportRequest;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetAllAttendanceRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id,GetAttendanceRequest $request)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAttendanceRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttendanceRequest $request, $id)
    {
        $request->id = $id;
        
        $response = $request->handle();

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteAttendanceRequest $request,$id)
    {
        $request->id = $id;

        $response = $request->handle();
        
        return response()->json($response);
    }  
    public function getReport(GetReportRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }
    public function getMonthWiseReport(GetMonthWiseReportRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }
    public function getAcademicYearReport(GetAcademicYearReportRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }
    public function getAcademicMonthReport(GetAcademicMonthReportRequest $request)
    {
        $response = $request->handle();

        return response()->json($response);
    }
}