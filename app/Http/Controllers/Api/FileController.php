<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileController extends Controller
{
 
    public function upload(Request $request)
    {
        if ($request->file('image')) {

            //($request->file('image'));
             
            //store file into document folder
            $path = $request->file('image')->store('images');
            
            //store your file into database
            //$document = new Document();
            //$document->title = $path;
            //$document->save();

            $url = url('storage/uploads/' . $path);

            $url = str_replace('public/','',$url);
              
            return Response()->json([
                "success" => true,
                "path" => $url
            ]);
  
        }
        return Response()->json([
                "success" => false,
                "file" => ''
          ]);
  
    }  
}