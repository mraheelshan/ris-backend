<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{
    public $table = 'academic_year';
    protected $guarded = ['id'];
    public $timestamps = false;
     
}
