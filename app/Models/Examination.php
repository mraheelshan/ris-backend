<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    public $table = 'examinations';
    protected $guarded = ['id'];
    public $timestamps = true;
    public $appends = ['school', 'term', 'class'];

    public function schoolType()
    {
        return $this->belongsTo(\App\Models\School::class, 'school_id');
    }
    public function termType()
    {
        return $this->belongsTo(\App\Models\Terms::class, 'term_id');
    }
    public function classType()
    {
        return $this->belongsTo(\App\Models\Classes::class, 'class_id');
    }
    public function getSchoolAttribute(){
        return $this->schoolType->name ?? '-';
    }
    public function getClassAttribute(){
        return $this->classType->name ?? '-';
    }
    public function getTermAttribute(){
        return $this->termType->name ?? '-';
    }
    public function courses(){
        return $this->hasMany(\App\Models\CoursesExamination::class);
    }
}
