<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    public $table = 'schools';
    protected $guarded = ['id'];
    protected $appends = ['school-type','donor'];
    
    public function type()
    {
        return $this->belongsTo(\App\Models\SchoolTypes::class, 'school_type_id');
    }

    public function donorname()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'donor_id');
    }

    public function getSchoolTypeAttribute()
    {
        return  $this->type->name ?? '-';
    }
    public function getdonorAttribute()
    {
        return  $this->donorname->name ?? '-';
    }
}
