<?php

namespace App\Models;

class Role extends BaseModel
{
    public $table = 'roles';
    protected $guarded = ['id'];
}