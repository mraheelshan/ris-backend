<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentParents extends Model
{
    public $table = 'student_parent';
    protected $guarded = ['id'];
    public $timestamps = false;
     
}
