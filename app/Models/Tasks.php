<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    public $table = 'user_task';
    protected $guarded = ['id'];
    public $timestamps = true;
    public $appends = ['user'];


    public function username()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'user_id');
    }
    public function getUserAttribute()
    {
        return  $this->username->name ?? '-';
    }
}
