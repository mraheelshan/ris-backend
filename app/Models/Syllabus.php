<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Syllabus extends Model
{
    public $table = 'syllabus';
    protected $guarded = ['id'];
    public $timestamps = true;
    public $appends = ['class'];


    public function classname(){
        return $this->belongsTo(\App\Models\Classes::class, 'class_id');
    }
    // public function username(){
    //     return $this->belongsTo(\App\Models\User::class, 'user_id');
    // }
    public function getClassAttribute()
    {
        return  $this->classname->name ?? '-';
    }
  
}
