<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    public $table = 'student_family';
    protected $guarded = ['id'];
    public $timestamps = false;
}
