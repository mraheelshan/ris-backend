<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    public $table = 'classes';
    protected $guarded = ['id'];
    // public $timestamps = false;

    public function course()
    {
        return $this->belongsToMany(Course::class);
    }
}
