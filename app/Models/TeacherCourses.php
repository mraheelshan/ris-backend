<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherCourses extends Model
{
    public $table = 'teacher_courses';
    protected $guarded = ['id'];
    public $timestamps = false;
}
