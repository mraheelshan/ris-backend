<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursesExamination extends Model
{
    public $table = 'course_examination';
    protected $guarded = ['id'];
    public $timestamps = false;
     
}
