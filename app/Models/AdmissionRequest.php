<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdmissionRequest extends Model
{
    public $table = 'admission_request';
    protected $guarded = ['id'];
    public $timestamps = false;
}
