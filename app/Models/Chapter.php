<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    public $table = 'chapters';
    protected $guarded = ['id'];
    public $timestamps = true;
    protected $appends = ['book'];
    
    public function books()
    {
        return $this->belongsTo(\App\Models\Book::class, 'book_id');
    }
    public function getBookAttribute()
    {
        return  $this->books->name;
    }
}
