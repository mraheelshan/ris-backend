<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder\Class_;

class Course extends Model
{
    public $table = 'courses';
    protected $guarded = ['id'];
    public $timestamps = false;
     

    public function class()
    {
        return $this->belongsToMany(Classes::class);
    }
}
