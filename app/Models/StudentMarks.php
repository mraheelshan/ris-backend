<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentMarks extends Model
{
    public $table = 'student_marks';
    protected $guarded = ['id'];
    public $timestamps = true;
}
