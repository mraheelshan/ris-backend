<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $table = 'students';
    protected $guarded = ['id'];
    public $timestamps = true;
    protected $appends = ['school-name', 'class'];

    public function school()
    {
        return $this->belongsTo(\App\Models\School::class, 'school_id');
    }
    public function classes()
    {
        return $this->belongsTo(\App\Models\Classes::class, 'class_id');
    }
    public function getSchoolNameAttribute()
    {
        return  $this->school->name  ?? '-';
    }
    public function getClassAttribute()
    {
        return  $this->classes->name  ?? '-';
    }

    public function attendences()
    {
        return $this->hasMany(\App\Models\Attendance::class);
    }

    public function getAttendence($month, $year)
    {
        return $this->attendences()->where('month', $month)->where('year', $year)->orderBy('day', 'asc')->get();
    }
    public function getMonthAttendence( $to, $from, $year)
    {
        return  $this->attendences()->whereBetween('month', [$from, $to])->where('year', $year)->orderBy('month', 'asc')->orderBy('day', 'asc')->get();
    }
}
