<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use App\Scopes\AgeScope;
// use App\Events\Category\CategoryDeleted;
// use App\Events\Category\CategorySaved;
use App\Observers\CategoryObserver;

class Category extends Model
{
    protected $table = 'category';
    // protected $primaryKey = 'id';
    //public $incrementing = false;
    //protected $keyType = 'string';
	public $timestamps = false;	//created_at , updated_at 
	//protected $dateFormat = 'U';
	//const CREATED_AT = 'creation_date';
    //const UPDATED_AT = 'last_update';
    //protected $connection = 'connection-name';
	protected $attributes = [
        'active' => true,
        'parent_id' => 0,
    ];
    protected $guarded = ['id'];

    protected $casts = [
        'active' => 'boolean',
    ];     

    // protected $appends = ['parent_category'];


    // protected $fillable = ['name','active','image','description','meta_description','meta_keywords','meta_title','parent_id']; // only accept these and ignore others
    //protected $guarded = ['id']; // revers of $fillable
    // protected $hidden = ['locale','parent_id','parent'];
    //protected $visible = ['first_name', 'last_name']; // alternate of $hidden

    // public function getActiveAttribute($value){
    //     return $value ? 'Yes' : 'No';
    // }

    public function getParentCategoryAttribute(){
        return $this->parent->name ?? '-';
    }

    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');//->withDefault();
    }

    // public function child()
    // {
    //     // recursively return all children
    //     return $this->hasOne(Category::class, 'parent_id');
    // }    

    // public function children()
    // {
    //     return $this->hasMany('App\Models\Category','parent_id');
    // }    

    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }
 
    // This is method where we implement recursive relationship
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->with('categories');
    }    

    // public function comments()
    // {
    // return $this->hasMany(Comment::class);
    // }
    // public function approved_comments()
    // {
    // return $this->hasMany(Comment::class)->where('approved', 1);
    // }

    //use SoftDeletes; // must have a column in db table deleted_at , query will exclude all soft deleted models, use $flight->trashed() to see if model is soft deleted
    // $flights = App\Flight::withTrashed()->where('account_id', 1)->get();
    //$flight->history()->withTrashed()->get(); 
    //$flights = App\Flight::onlyTrashed()->where('airline_id', 1)->get();
    //App\Flight::withTrashed()->where('airline_id', 1)->restore();
    //$flight->history()->restore(); relation
	// Skip soft delete and permanently delete
	//$flight->forceDelete();

	// Force deleting all related models...
	//$flight->history()->forceDelete();  
	/*  
    protected static function booted()
    {
        static::addGlobalScope(new AgeScope);
    }
    //Anonymous
    protected static function booted()
    {
        static::addGlobalScope('age', function (Builder $builder) {
            $builder->where('age', '>', 200);
        });
    }  

    User::withoutGlobalScope(AgeScope::class)->get();
	User::withoutGlobalScope('age')->get(); // from closure
	// Remove all of the global scopes...
	User::withoutGlobalScopes()->get();

	// Remove some of the global scopes...
	User::withoutGlobalScopes([
	    FirstScope::class, SecondScope::class
	])->get();

    */  

    /*
	Local Scope
	public function scopePopular($query)
    {
        return $query->where('votes', '>', 100);
    }	
 	public function scopeActive($query)
    {
        return $query->where('active', 1);
    } 

    $users = App\User::popular()->active()->orderBy('created_at')->get();

	$users = App\User::popular()->orWhere(function (Builder $query) {
	    $query->active();
	})->get();
	// or
	$users = App\User::popular()->orWhere->active()->get();

    */

    /*
	Dynamic
 	public function scopeOfType($query, $type)
    {
        return $query->where('type', $type);
    }	

    $users = App\User::ofType('admin')->get();
    */

    /*
	Events
	retrieved, creating, created, updating, updated, saving, saved, deleting, deleted, restoring, restored
    
    protected $dispatchesEvents = [
        'saved' => UserSaved::class,
        'deleted' => UserDeleted::class,
    ];

    Alternate
    */
    // protected static function booted()
    // {
    //     static::observe(CategoryObserver::class);
    // }    
    /* execute code without firing events
	$user = User::withoutEvents(function () use () {
	    User::findOrFail(1)->delete();

	    return User::find(2);
	});
    */

    /*
    // Alternate of $dispatchEvents
	public static function boot()
	{
	    parent::boot();
	    static::observe(CategoryObserver::class);
	}
    */
}
