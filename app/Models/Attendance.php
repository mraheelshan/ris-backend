<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public $table = 'student_attendance';
    protected $guarded = ['id','leave-reasone'];
    // protected $guarded = ['leave-reasone'];
    public $timestamps = true;
     
}
