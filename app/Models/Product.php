<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'product';
    protected $guarded = ['id'];
    public $timestamps = false;

    protected $appends = ['image'];
    protected $with = ['images'];

    protected $casts = [
        'is_main' => 'boolean',
        'active' => 'boolean',
        'has_variants' => 'boolean',
        'unlimited_stock' => 'boolean',
        'offer' => 'boolean',
    ];    

    public function category(){
    	return $this->belongsTo(\App\Models\Category::class);
    }

    public function brand(){
    	return $this->belongsTo(\App\Models\Brand::class);
    }
    
    public function images(){
        return $this->hasMany(\App\Models\Image::class);
    }

    public function getImageAttribute()
    {
        $images = $this->images;

        if(sizeof($images) > 0){
            return $images[0]->url;
        }else{
            return '';
        }
    }
}
