<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolTypes extends Model
{
    public $table = 'school_types';
    protected $guarded = ['id'];
    public $timestamps = false;
     
}
