<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aid extends Model
{
    public $table = 'aid';
    protected $guarded = ['id'];
    public $timestamps = true;
    public $appends = ['donor', 'student'];

    public function donorType()
    {
        return $this->belongsTo(\App\Models\Profile::class, 'donor_id');
    }
    public function studentType()
    {
        return $this->belongsTo(\App\Models\Student::class, 'student_id');
    }
    public function getDonorAttribute(){
        return $this->donorType->name ?? '-';
    }
    public function getStudentAttribute(){
        return $this->studentType->first_name ?? '-';
    }
}
