<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $table = 'profile';
    protected $guarded = ['id'];
    public $timestamps = true;

    public function course(){
        return $this->belongsToMany(\App\Models\Course::class,'teacher_courses','user_id','course_id');
    }

    public function class(){
        return  $this->belongsToMany(\App\Models\Classes::class,'teacher_courses','user_id','class_id');
    }
    public function courses(){
        return $this->hasMany(\App\Models\TeacherCourses::class,'user_id');
    }
}
