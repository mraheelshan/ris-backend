<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public $table = 'books';
    protected $guarded = ['id'];
    protected $appends = ['course','class'];

    public function courses()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }
    public function classes()
    {
        return $this->belongsTo(\App\Models\Classes::class, 'class_id');
    }
    public function getCourseAttribute()
    {
        return  $this->courses->name  ?? '-';
    }
    public function getClassAttribute()
    {
        return  $this->classes->name  ?? '-';
    }
}
