<?php

use App\Http\Controllers\Api\FileController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors', 'auth:api']], function () {
	Route::resource('academic', 'AcademicController');
	Route::resource('school-type', 'SchoolTypesController');
	Route::resource('classes', 'ClassesController');
	Route::resource('schools', 'SchoolController');
	Route::resource('courses', 'CourseController');
	Route::resource('students', 'StudentController');
	Route::resource('books', 'BookController');
	Route::resource('chapters', 'ChapterController');
	Route::resource('syllabus', 'SyllabusController');
	Route::resource('admission-request', 'AdmissionRequestController');
	Route::resource('family', 'FamilyController');
	Route::resource('aid', 'AidController');
	Route::resource('terms', 'TermsController');
	Route::resource('examination', 'ExaminationController');
	Route::resource('tasks', 'TasksController');
	Route::resource('task-status', 'TaskStatusController');
	Route::resource('role', 'RoleController');
	Route::resource('profile', 'ProfileController');
	Route::resource('permissions', 'PermissionController');
	Route::resource('attendance', 'AttendanceController');

	Route::get('school-classes/{id}', 'SchoolController@getClasses');
	Route::get('profile-classes', 'ProfileController@getClasses');
	Route::get('class-students/{id}', 'StudentController@getStudents');
	Route::get('profile-exams/{id}', 'ExaminationController@getClasses');
	Route::post('attempt-exam', 'ExaminationController@getCourses');
	Route::post('attempt', 'ExaminationController@examStore');

	Route::post('attendance-report', 'AttendanceController@getReport');
	Route::post('month-wise-attendance', 'AttendanceController@getMonthWiseReport');
	Route::post('academic-year-report', 'AttendanceController@getAcademicYearReport');
	Route::post('academic-month-report', 'AttendanceController@getAcademicMonthReport');


	Route::post('file/upload', [FileController::class, 'upload']);
});

