<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Classes;
use Faker\Generator as Faker;

$factory->define(Classes::class, function (Faker $faker) {
    return [
        'name' => $this->faker->title,
        'roman' => $this->faker->text,
        'status' => 1
    ];
});
