<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <a class="nav-link" href="index.html">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Dashboard
            </a>
            <div class="sb-sidenav-menu-heading">Operations</div>
            <a class="nav-link" href="charts.html">
                <div class="sb-nav-link-icon"><i class="fas fa-shopping-cart"></i></div>
                Orders
            </a>   
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                <div class="sb-nav-link-icon"><i class="fas fa-chart-bar"></i></div>
                Reports
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
         
            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{ route('permissions.index') }}">Sales Report</a>
                    <a class="nav-link" href="{{ route('roles.index') }}">Stock Report</a>
                    <a class="nav-link" href="{{ route('users.index') }}">Peak Time Report</a>
                    <a class="nav-link" href="{{ route('users.index') }}">User Report</a>
                </nav>
            </div> 
            <a class="nav-link" href="charts.html">
                <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                Customers
            </a>   
            <a class="nav-link" href="charts.html">
                <div class="sb-nav-link-icon"><i class="fas fa-tag"></i></div>
                Promotions
            </a>
            <a class="nav-link" href="charts.html">
                <div class="sb-nav-link-icon"><i class="fas fa-envelope-open-text"></i></div>
                Newsletter Subscriptions
            </a>
            <div class="sb-sidenav-menu-heading">Setup</div>
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts1" aria-expanded="false" aria-controls="collapseLayouts1">
                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                Catalog
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse" id="collapseLayouts1" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{ route('users.index') }}"> Attributes</a>
                    <a class="nav-link" href="{{ route('permissions.index') }}"> Brand</a>
                    <a class="nav-link" href="{{ route('roles.index') }}">Category</a>
                    <a class="nav-link" href="{{ route('users.index') }}">Product</a>
                </nav>
            </div>   
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapseLayouts2">
                <div class="sb-nav-link-icon"><i class="fas fa-desktop"></i></div>
                CMS
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{ route('users.index') }}">Banners</a>
                    <a class="nav-link" href="{{ route('permissions.index') }}">Page</a>
                    <a class="nav-link" href="{{ route('roles.index') }}">Payment Methods</a>
                    <a class="nav-link" href="{{ route('users.index') }}">Shipping Methods</a>
                </nav>
            </div>
            <a class="nav-link" href="index.html">
                <div class="sb-nav-link-icon"><i class="fas fa-store"></i></div>
                Store
            </a> 
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts3" aria-expanded="false" aria-controls="collapseLayouts3">
                <div class="sb-nav-link-icon"><i class="fas fa-users-cog"></i></div>
                User Management
                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
            </a>
            <div class="collapse" id="collapseLayouts3" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{ route('permissions.index') }}">Permissions</a>
                    <a class="nav-link" href="{{ route('roles.index') }}">Roles</a>
                    <a class="nav-link" href="{{ route('users.index') }}">Users</a>
                </nav>
            </div>
          <!--   <a class="nav-link" href="index.html">
                <div class="sb-nav-link-icon"><i class="fas fa-cog"></i></div>
                Settings
            </a>   -->
        </div>
    </div>
    <!-- <div class="sb-sidenav-footer">
        <div class="small">Logged in as:</div>
        {{ Auth::user()->name }}
    </div> -->
</nav>